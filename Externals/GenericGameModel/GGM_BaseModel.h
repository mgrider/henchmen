//
//  Generic_GameModel.h
//  puzalution
//
//  Created by Martin Grider on 8/24/12.
//  Copyright (c) 2012 Abstract Puzzle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"


@interface GGM_BaseModel : BaseModel


// grid size
@property (assign) int gridWidth;
@property (assign) int gridHeight;

// gridElements contains MutableArrays for each row,
// in turn containing NSNumbers representing each element's "state"
@property (nonatomic, strong) NSMutableArray *gridElements;

// game states are between 0 and gridMaxStateInt
@property (assign) int gridMaxStateInt;

// is the game over?
@property (assign) BOOL gameIsOver;

// score support
@property (assign) int score;


+ (instancetype)instanceWithWidth:(int)width andHeight:(int)height;
- (void)buildStateObjects;

- (void)setStateAtX:(int)x andY:(int)y toState:(int)newState;
- (int)randomStateInt;
- (void)randomizeGridFromMaxPossibleState;
- (void)setAllStatesTo:(int)stateInt;

- (int)stateAtX:(int)x andY:(int)y;

- (void)incrementScore;


@end
