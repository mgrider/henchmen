//
//  Generic_GameModel.m
//  puzalution
//
//  Created by Martin Grider on 8/24/12.
//  Copyright (c) 2012 Abstract Puzzle. All rights reserved.
//

#import "GGM_BaseModel.h"
#import "BaseModel.h"


@implementation GGM_BaseModel

@synthesize gridHeight = gridHeight_;
@synthesize gridWidth = gridWidth_;
@synthesize gridElements = gridElements_;
@synthesize gridMaxStateInt = gridMaxStateInt_;

@synthesize gameIsOver = gameIsOver_;
@synthesize score = score_;


#pragma mark - creation / init

+ (instancetype)instanceWithWidth:(int)width andHeight:(int)height
{
	GGM_BaseModel *model = [self instance];
	model.gridHeight = height;
	model.gridWidth = width;

	[model buildStateObjects];

	return model;
}

- (void)buildStateObjects
{
	// jumping off point for custom initialization if you want it.

	// init the gridElements
	self.gridElements = [[NSMutableArray alloc] initWithCapacity:self.gridHeight];
	NSMutableArray *subarray;
	for (int y=0; y < self.gridHeight; y++) {
		subarray = [[NSMutableArray alloc] initWithCapacity:self.gridWidth];
		for (int x = 0; x < self.gridWidth; x++) {
			[subarray insertObject:[NSNumber numberWithInt:0] atIndex:x];
		}
		[self.gridElements addObject:subarray];
	}
}


#pragma mark - setting game state

- (void)setStateAtX:(int)x andY:(int)y toState:(int)newState
{
	if (x < 0 ||
		y < 0 ||
		x >= gridWidth_ ||
		y >= gridHeight_) {
		NSLog(@"THIS IS A FAIL at x:%i andy:%i.", x, y );
		return;
	}
	[[self.gridElements objectAtIndex:y] replaceObjectAtIndex:x withObject:[NSNumber numberWithInt:newState]];
}

- (int)randomStateInt
{
	return arc4random() % (gridMaxStateInt_ + 1);
}

- (void)randomizeGridFromMaxPossibleState
{
	NSNumber *state;
	for (int y=0; y < self.gridHeight; y++) {
		for (int x=0; x < self.gridWidth; x++) {
			state = [NSNumber numberWithInt:[self randomStateInt]];
			[[self.gridElements objectAtIndex:y] replaceObjectAtIndex:x withObject:state];
		}
	}
}

- (void)setAllStatesTo:(int)stateInt
{
	NSNumber *state;
	for (int y=0; y < self.gridHeight; y++) {
//		NSLog(@"y is %i", y);
		for (int x=0; x < self.gridWidth; x++) {
			state = [NSNumber numberWithInt:stateInt];
			[[self.gridElements objectAtIndex:y] replaceObjectAtIndex:x withObject:state];
		}
	}
}


#pragma mark - querrying game state

- (int)stateAtX:(int)x andY:(int)y
{
	if (x < 0 ||
		y < 0 ||
		x >= gridWidth_ ||
		y >= gridHeight_) {
		return -1;
	}
	return [[[self.gridElements objectAtIndex:y] objectAtIndex:x] intValue];
}


#pragma mark - score

- (void)incrementScore
{
	self.score = self.score + 1;
}


#pragma mark - desc

- (NSString *)description
{
	NSString *desc = @"Game Model:\n";
	for (int i=0; i<self.gridHeight; i++) {
		desc = [desc stringByAppendingString:@"[ "];
		for (int x=0; x < self.gridWidth; x++) {
			desc = [desc stringByAppendingFormat:@"%@, ", [[self.gridElements objectAtIndex:i] objectAtIndex:x]];
		}
		desc = [desc stringByAppendingFormat:@"]\n"];
	}
	return desc;
}

@end
