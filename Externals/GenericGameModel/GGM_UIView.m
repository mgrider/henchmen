//
//  GGM_UIView.m
//  puzalution
//
//  Created by Martin Grider on 8/26/12.
//  Copyright (c) 2012 Abstract Puzzle. All rights reserved.
//

#import "GGM_UIView.h"
#import "GGM_BaseModel.h"
#import <QuartzCore/QuartzCore.h>


@implementation GGM_UIView

@synthesize game = game_;

@synthesize recognizesTaps = recognizesTaps_;
@synthesize tapGestureRecognizer = tapGestureRecognizer_;

@synthesize recognizesDrags = recognizesDrags_;
@synthesize dragGestureRecognizer = dragGestureRecognizer_;
@synthesize dragPointBegan = dragPointBegan_;
@synthesize dragPointEnded = dragPointEnded_;
@synthesize dragPointCurrent = dragPointCurrent_;
@synthesize isDragging = isDragging_;

@synthesize gridPixelHeight = gridPixelHeight_;
@synthesize gridPixelWidth = gridPixelWidth_;
@synthesize gridViewArray = gridViewArray_;
@synthesize gridType = gridType_;


#pragma mark - tap touch detection

- (BOOL)recognizesTaps
{
	return recognizesTaps_;
}

- (void)setRecognizesTaps:(BOOL)recognizesTaps
{
	recognizesTaps_ = recognizesTaps;
	if (recognizesTaps) {
		tapGestureRecognizer_ = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
		[tapGestureRecognizer_ setNumberOfTapsRequired:1];
		[tapGestureRecognizer_ setNumberOfTouchesRequired:1];
		[tapGestureRecognizer_ setEnabled:YES];
//		[tapGestureRecognizer_ requireGestureRecognizerToFail:doubleTapGestureRecognizer];
		[self addGestureRecognizer:tapGestureRecognizer_];
	}
}

- (void)handleTap:(UITapGestureRecognizer *)sender
{
	CGPoint tapPoint = [sender locationInView:self];

	int x = tapPoint.x / gridPixelWidth_;
	int y = tapPoint.y / gridPixelHeight_;

	int state = [self.game stateAtX:x andY:y];

	NSLog(@"tap discovered at: %@, coords: {%i, %i} with state: %i", NSStringFromCGPoint(tapPoint), x, y, state);

	[self handleTapAtX:x andY:y];
}

- (void)handleTapAtX:(int)x andY:(int)y
{
	// to implement in subclasses
	NSLog(@"in GGM_UIView.m ... handleTapAtX");
}


#pragma mark - drag touch detection

- (BOOL)recognizesDrags
{
	return recognizesDrags_;
}

- (void)setRecognizesDrags:(BOOL)recognizesDrags
{
	NSLog(@"recognizes drags is %d", recognizesDrags);
	recognizesDrags_ = recognizesDrags;
	if (recognizesDrags) {
		dragGestureRecognizer_ = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleDrag:)];
		[self addGestureRecognizer:dragGestureRecognizer_];
	}
}

- (void)handleDrag:(UIPanGestureRecognizer*)sender
{
	CGPoint dragPoint = [sender locationInView:self];
	int x = dragPoint.x / gridPixelWidth_;
	int y = dragPoint.y / gridPixelHeight_;
	if (sender.state == UIGestureRecognizerStateBegan) {
		dragPointBegan_ = dragPoint;
		isDragging_ = YES;
		NSLog(@"pan started at: %@, coords are: {%i, %i}", NSStringFromCGPoint(dragPoint), x, y);
	}
	else if (sender.state == UIGestureRecognizerStateEnded) {
		dragPointEnded_ = dragPoint;
		isDragging_ = NO;
		NSLog(@"pan ended at: %@, coords are: {%i, %i}", NSStringFromCGPoint(dragPoint), x, y);
		[self handleEndDrag];
	}
	else {
		dragPointCurrent_ = dragPoint;
	}
}

- (void)handleEndDrag
{
	// implement in subclasses
}

- (BOOL)dragAllowedInDirection:(GGM_MoveDirection)direction fromX:(int)x andY:(int)y
{
	// implementin subclasses
	return YES;
}


#pragma mark - convenience

- (UIView*)viewForX:(int)x andY:(int)y
{
	return [[self.gridViewArray objectAtIndex:y] objectAtIndex:x];
}


#pragma mark - drawing

- (void)refreshViewPositionsAndStates
{
	int gameState = 0;
	UIView *view;
	UIImage *image;
	for (int y = 0; y < self.game.gridHeight; y++)
	{
		for (int x = 0; x < self.game.gridWidth; x++)
		{
			gameState = [self.game stateAtX:x andY:y];
			view = [[self.gridViewArray objectAtIndex:y] objectAtIndex:x];
			[view setFrame:CGRectMake((gridPixelWidth_*x), (gridPixelHeight_*y), gridPixelWidth_, gridPixelHeight_)];

			switch (self.gridType) {
				case GGM_GRIDTYPE_COLOR: {
					[view setBackgroundColor:[self colorForGameState:gameState]];
					break;
				}
				case GGM_GRIDTYPE_IMAGE: {
					image = [self imageForGameState:gameState];
					[(UIImageView*)view setImage:image];
					if(image != nil) {
						NSLog(@"x:%i y:%i is %@", x, y, view);
					}
					break;
				}
				case GGM_GRIDTYPE_TEXTLABEL: {
					[(UILabel *)view setText:[self textForGameState:gameState]];
					break;
				}
			}
		}
	}
}

- (UIImage*)imageForGameState:(int)stateInt
{
	// implement in your subclass.
	switch (stateInt) {
//		case 1:
//			return [UIImage imageNamed:@"cloud320blue.png"];
//		case 2:
//			return [UIImage imageNamed:@"cloud320green.png"];
//		case 3:
//			return [UIImage imageNamed:@"cloud320red.png"];
		case 0:
		default:
			return nil;
	}
}

- (UIColor *)colorForGameState:(int)stateInt
{
	switch (stateInt) {
//		case 1:
//			return [UIColor redColor];
//		case 2:
//			return [UIColor greenColor];
//		case 3:
//			return [UIColor blueColor];

		case 0:
		default:
			return [UIColor clearColor];
	}
}

- (NSString *)textForGameState:(int)stateInt
{
	// implement in subclasses
	return @"";
}

- (UIView *)newSubviewForGameState:(int)state
{
	switch (gridType_) {

		case GGM_GRIDTYPE_COLOR: {
			UIView *view = [[UIView alloc] init];
			[view setBackgroundColor:[self colorForGameState:state]];
			return view;
		}

		case GGM_GRIDTYPE_IMAGE: {
			UIImage *image;
			UIImageView *imageView;
			image = [self imageForGameState:state];
			imageView = [[UIImageView alloc] initWithImage:image];
			return imageView;
		}

		case GGM_GRIDTYPE_TEXTLABEL: {
			UILabel *label = [[UILabel alloc] init];
			[label setNumberOfLines:3];
			[label setAdjustsFontSizeToFitWidth:YES];
			[label setLineBreakMode:NSLineBreakByWordWrapping];
			[label setTextAlignment:NSTextAlignmentCenter];
			[label setMinimumScaleFactor:0.5f];
			[label setFont:[UIFont systemFontOfSize:11.0f]];
			[label setText:[self textForGameState:state]];
			[label.layer setBorderColor:[UIColor darkGrayColor].CGColor];
			[label.layer setBorderWidth:3.0f];
			return label;
		}
	}
	return [[UIView alloc] init];
}


#pragma mark - dealing with the gameModel

- (GGM_BaseModel *)game
{
	return game_;
}

- (void)setGame:(GGM_BaseModel *)game
{
	// for most purposes, this is our init
	game_ = game;
	[self setupInitialGridViewArray];
}

- (void)setupInitialGridViewArray
{
	gridPixelWidth_ = self.frame.size.width / game_.gridWidth;
	gridPixelHeight_ =  self.frame.size.width / game_.gridHeight;

	gridViewArray_ = [NSMutableArray arrayWithCapacity:game_.gridHeight];

	NSMutableArray *subarray;
	int gameState = 0;
	UIView *view;
	for (int y = 0; y < game_.gridHeight; y++) {

		subarray = [NSMutableArray arrayWithCapacity:game_.gridWidth];

		for (int x = 0; x < game_.gridWidth; x++) {

			gameState = [game_ stateAtX:x andY:y];
			view = (UIView*)[self newSubviewForGameState:gameState];
			[view setFrame:CGRectMake((gridPixelWidth_*x), (gridPixelHeight_*y), gridPixelWidth_, gridPixelHeight_)];

			[self addSubview:view];
			[subarray insertObject:view atIndex:x];
		}
		[gridViewArray_ insertObject:subarray atIndex:y];
	}
}


#pragma mark - inherited

//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        // Initialization code
//    }
//    return self;
//}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
