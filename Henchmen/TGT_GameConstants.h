//
//  TGT_GameConstants.h
//  Henchmen
//
//  Created by Martin Grider on 12/15/12.
//  Copyright (c) 2012 Abstract Puzzle. All rights reserved.
//

#ifndef Henchmen_TGT_GameConstants_h
#define Henchmen_TGT_GameConstants_h


typedef enum
{
	TGT_GAME_WALKABLE_TILE_EMPTY,
	TGT_GAME_WALKABLE_TILE_EMPTY_HIGHLIGHT,
	TGT_GAME_WALKABLE_TILE_OPENED_GATE_HORIZONTAL,
	TGT_GAME_WALKABLE_TILE_OPENED_GATE_VERTICAL,
	TGT_GAME_WALKABLE_TILE_PIT_TRAP_OCCUPIED_NORTH,
	TGT_GAME_WALKABLE_TILE_PIT_TRAP_OCCUPIED_SOUTH,
	TGT_GAME_WALKABLE_TILE_PIT_TRAP_OCCUPIED_EAST,
	TGT_GAME_WALKABLE_TILE_PIT_TRAP_OCCUPIED_WEST,
	TGT_GAME_BLOCKING_TILE_NONE,
	TGT_GAME_BLOCKING_TILE_PIT_TRAP,
	TGT_GAME_BLOCKING_TILE_HENCHMAN_OCCUPIED,
	TGT_GAME_BLOCKING_TILE_GATE_HORIZONTAL,
	TGT_GAME_BLOCKING_TILE_GATE_VERTICAL,
	TGT_GAME_GOAL_TILE_GOLD
} TGT_Game_TileType;

typedef enum
{
	TGT_GAME_UI_STATE_IDLE,
	TGT_GAME_UI_STATE_ANIMATING_MOVE,
	TGT_GAME_UI_STATE_WON
} TGT_Game_UI_StateType;


#define kGameConfig_hasCutScene @"kGameConfig_hasCutScene"
#define kGameConfig_cutSceneText @"kGameConfig_cutSceneText"
#define kGameConfig_width @"kGameConfig_width"
#define kGameConfig_height @"kGameConfig_height"
#define kGameConfig_specialTilesArray @"kGameConfig_specialTilesArray"


#endif
