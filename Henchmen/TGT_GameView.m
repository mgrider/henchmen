//
//  TGT_GameView.m
//  Henchmen
//
//  Created by Martin Grider on 12/15/12.
//  Copyright (c) 2012 Abstract Puzzle. All rights reserved.
//

#import "TGT_GameView.h"
#import "TGT_GameConstants.h"
#import "TGT_Game.h"


#define kTGT_ViewDict_x @"kTGT_ViewDict_x"
#define kTGT_ViewDict_y @"kTGT_ViewDict_y"


@implementation TGT_GameView

@synthesize dragCoordinateDictArray = dragCoordinateDictArray_;
@synthesize uIState = uIState_;
@synthesize extraViews = extraViews_;


#pragma mark - user interaction

// not used
- (void)handleTapAtX:(int)x andY:(int)y
{
	// for now set teh state
	[self.game setStateAtX:x andY:y toState:1];
	[self refreshViewPositionsAndStates];
}

- (void)handleDragAtX:(int)x andY:(int)y
{
	if (uIState_ != TGT_GAME_UI_STATE_IDLE) {
		return;
	}

	// if we don't already have a drag in progress...
	if ([self.dragCoordinateDictArray count] == 0) {
		// make sure we are at the entry point
		if (x != self.game.entryX || y != self.game.entryY) {
			return;
		}
	}
	else {
		NSDictionary *lastDict = [self.dragCoordinateDictArray lastObject];
		if (x == [[lastDict objectForKey:kTGT_ViewDict_x] intValue] &&
			y == [[lastDict objectForKey:kTGT_ViewDict_y] intValue]) {
			// only need to add a given coord once
			return;
		}
	}

	// what to do for each tile type
	BOOL endsDrag = NO;
	int state = [self.game stateAtX:x andY:y];
	switch (state) {
		case TGT_GAME_WALKABLE_TILE_EMPTY_HIGHLIGHT:
		case TGT_GAME_WALKABLE_TILE_EMPTY: {
			// highlight this empty space
			[self.game setStateAtX:x andY:y toState:TGT_GAME_WALKABLE_TILE_EMPTY_HIGHLIGHT];
			// add the space to the dragCoordinateDictArray
			[self.dragCoordinateDictArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:
													 [NSNumber numberWithInt:x], kTGT_ViewDict_x,
													 [NSNumber numberWithInt:y], kTGT_ViewDict_y,
													 nil]];
			break;
		}

			// some states will just end the drag
		case TGT_GAME_BLOCKING_TILE_PIT_TRAP: {
			endsDrag = YES;
			[self.dragCoordinateDictArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:
													 [NSNumber numberWithInt:x], kTGT_ViewDict_x,
													 [NSNumber numberWithInt:y], kTGT_ViewDict_y,
													 nil]];
			break;
		}

			// some states will end the drag on the previous location
		case TGT_GAME_BLOCKING_TILE_NONE: {
			endsDrag = YES;
			break;
		}

		default:
			break;
	}

	// always redraw
	[self refreshViewPositionsAndStates];

	// before possibly ending the drag
	if (endsDrag) {
		[self handleEndDrag];
	}
}

- (void)handleEndDrag
{
	int dragCount = [self.dragCoordinateDictArray count];
	if (uIState_ != TGT_GAME_UI_STATE_IDLE ||
		dragCount <= 0) {
		return;
	}

	uIState_ = TGT_GAME_UI_STATE_ANIMATING_MOVE;

	[self.game setMinionsUsed:(self.game.minionsUsed + 1)];
	[[NSNotificationCenter defaultCenter] postNotificationName:kTGT_MinionCountChangedNotification object:self];

	// animate a new henchmen to its final destination
//	NSDictionary *finalDict = [self.dragCoordinateDictArray lastObject];
//	int finalX = [[finalDict objectForKey:kTGT_ViewDict_x] intValue];
//	int finalY = [[finalDict objectForKey:kTGT_ViewDict_y] intValue];
	UIImageView *viewToAnimate = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Sprite_DR_1.png"]];

	CGPoint firstPoint = CGPointMake(260.0f, 360.0f);
	[viewToAnimate setFrame:CGRectMake(firstPoint.x, firstPoint.y, 116.0f, 116.0f)];
	[self addSubview:viewToAnimate];

	[self animateHenchman:viewToAnimate toCoordinateIndex:0];
//
//	float animationIncrement = 0.2f;
//	float animationDelay = 0.1f;
//	NSDictionary *nextDict;
//	UIView *nextSquareView;
//	int nextX, nextY;
//	CGPoint nextPoint;
//	for (int i=0; i < dragCount; i++) {
//		nextDict = [self.dragCoordinateDictArray objectAtIndex:i];
//		NSLog(@"about to animate dict: %@", nextDict);
//		nextX = [[nextDict objectForKey:kTGT_ViewDict_x] intValue];
//		nextY = [[nextDict objectForKey:kTGT_ViewDict_y] intValue];
//		nextSquareView = [self viewForX:nextX andY:nextY];
//		nextPoint = CGPointMake(nextSquareView.center.x, nextSquareView.center.y-29.0f);
//		// animate
//		[UIView animateWithDuration:animationIncrement
//							  delay:animationDelay
//							options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionBeginFromCurrentState
//						 animations:^{
//							 [viewToAnimate setCenter:nextPoint];
//						 }
//						 completion:^(BOOL finished) {
//							 if (nextX == finalX && nextY == finalY) {
//								 [viewToAnimate removeFromSuperview];
//								 [self endHenchmanAnimationAtX:nextX andY:nextX];
//							 }
//						 }];
//		animationDelay = animationDelay + animationIncrement;
//	}
}

- (void)animateHenchman:(UIView*)henchman toCoordinateIndex:(int)index
{
	float animationIncrement = 0.2f;
	NSDictionary *nextDict = [self.dragCoordinateDictArray objectAtIndex:index];
	NSLog(@"about to animate dict: %@", nextDict);
	int nextX = [[nextDict objectForKey:kTGT_ViewDict_x] intValue];
	int nextY = [[nextDict objectForKey:kTGT_ViewDict_y] intValue];
	UIView *nextSquareView = [self viewForX:nextX andY:nextY];
	CGPoint nextPoint = CGPointMake(nextSquareView.center.x, nextSquareView.center.y-29.0f);
	[UIView animateWithDuration:animationIncrement
					 animations:^{
						 [henchman setCenter:nextPoint];
					 }
					 completion:^(BOOL finished){
						 if (index == [self.dragCoordinateDictArray count] -1) {
							 [henchman removeFromSuperview];
							 [self endHenchmanAnimationAtX:nextX andY:nextY];
						 }
						 else {
							 [self animateHenchman:henchman toCoordinateIndex:index+1];
						 }
					 }];
}

- (void)endHenchmanAnimationAtX:(int)x andY:(int)y
{
	switch ([self.game stateAtX:x andY:y]) {
		case TGT_GAME_WALKABLE_TILE_EMPTY_HIGHLIGHT:
		case TGT_GAME_WALKABLE_TILE_EMPTY: {
			[self.game setStateAtX:x andY:y toState:TGT_GAME_BLOCKING_TILE_HENCHMAN_OCCUPIED];
			break;
		}
		case TGT_GAME_BLOCKING_TILE_PIT_TRAP: {
			[self.game setStateAtX:x andY:y toState:TGT_GAME_WALKABLE_TILE_PIT_TRAP_OCCUPIED_EAST];
			break;
		}
		default:
			break;
	}

	// loop through all the dragCoordinates, and reset hilighted states
	int thisX, thisY, thisState;
	for (NSDictionary *dict in self.dragCoordinateDictArray) {
		thisX = [[dict objectForKey:kTGT_ViewDict_x] intValue];
		thisY = [[dict objectForKey:kTGT_ViewDict_y] intValue];
		thisState = [self.game stateAtX:thisX andY:thisY];
		switch (thisState) {
			case TGT_GAME_WALKABLE_TILE_EMPTY_HIGHLIGHT:
				[self.game setStateAtX:thisX andY:thisY toState:TGT_GAME_WALKABLE_TILE_EMPTY];
				break;

			default:
				break;
		}
	}
	[self.dragCoordinateDictArray removeAllObjects];

	[self refreshViewPositionsAndStates];

	uIState_ = TGT_GAME_UI_STATE_IDLE;
}


#pragma mark - drawing

- (void)refreshViewPositionsAndStates
{
	[super refreshViewPositionsAndStates];

	// also need to make sure we create/show any extraViews
	int gameState = 0;
	int x, y;
	id view;
	UIImage *image;
	CGRect squareRect;
	for (NSDictionary *dict in self.viewDictInDrawOrderArray) {
		x = [[dict objectForKey:kGGM_IsoViewDict_x] intValue];
		y = [[dict objectForKey:kGGM_IsoViewDict_y] intValue];
		gameState = [self.game stateAtX:x andY:y];
		image = [self imageForExtraViewState:gameState];
		if (image != nil) {
			view = [[self.extraViews objectAtIndex:y] objectAtIndex:x];
			if (view == [NSNull null]) {
				view = [[UIImageView alloc] initWithImage:image];
				[[self.extraViews objectAtIndex:y] replaceObjectAtIndex:x withObject:view];
				// set the frame, based on state and normalPoint
				squareRect = [self viewForX:x andY:y].frame;
				[view setFrame:CGRectMake(squareRect.origin.x, squareRect.origin.y-squareRect.size.height, squareRect.size.width, squareRect.size.height*2.0f)];
				[self addSubview:view];
			}
			else {
				[(UIImageView*)view setImage:image];
			}
		}
	}
}

- (UIImage*)imageForGameState:(int)stateInt
{
//	TGT_GAME_WALKABLE_TILE_EMPTY,
//	TGT_GAME_WALKABLE_TILE_EMPTY_HIGHLIGHT,
//	TGT_GAME_WALKABLE_TILE_OPENED_GATE_HORIZONTAL,
//	TGT_GAME_WALKABLE_TILE_OPENED_GATE_VERTICAL,
//	TGT_GAME_WALKABLE_TILE_PIT_TRAP_OCCUPIED_NORTH,
//	TGT_GAME_WALKABLE_TILE_PIT_TRAP_OCCUPIED_SOUTH,
//	TGT_GAME_WALKABLE_TILE_PIT_TRAP_OCCUPIED_EAST,
//	TGT_GAME_WALKABLE_TILE_PIT_TRAP_OCCUPIED_WEST,
//	TGT_GAME_BLOCKING_TILE_NONE,
//	TGT_GAME_BLOCKING_TILE_PIT_TRAP,
//	TGT_GAME_BLOCKING_TILE_HENCHMAN_OCCUPIED,
//	TGT_GAME_BLOCKING_TILE_GATE_HORIZONTAL,
//	TGT_GAME_BLOCKING_TILE_GATE_VERTICAL,
//	TGT_GAME_GOAL_TILE_GOLD

	switch (stateInt) {
			// these have extraViews on top of them:
		case TGT_GAME_GOAL_TILE_GOLD:
		case TGT_GAME_BLOCKING_TILE_PIT_TRAP:
		case TGT_GAME_BLOCKING_TILE_HENCHMAN_OCCUPIED:
		case TGT_GAME_WALKABLE_TILE_PIT_TRAP_OCCUPIED_EAST:
		case TGT_GAME_WALKABLE_TILE_PIT_TRAP_OCCUPIED_WEST:
		case TGT_GAME_WALKABLE_TILE_PIT_TRAP_OCCUPIED_SOUTH:
		case TGT_GAME_WALKABLE_TILE_PIT_TRAP_OCCUPIED_NORTH:
			// ...so they show the default grid tile
		case TGT_GAME_WALKABLE_TILE_EMPTY: {
			return [UIImage imageNamed:@"Grid_Real.png"];
		}
		case TGT_GAME_WALKABLE_TILE_EMPTY_HIGHLIGHT:
			return [UIImage imageNamed:@"Grid_2.png"];
			// default is show nothing...
		case TGT_GAME_BLOCKING_TILE_NONE:
		default:
			return nil;
	}
}

- (UIImage *)imageForExtraViewState:(int)stateInt
{
	switch (stateInt) {
			// these have extraViews on top of them:
		case TGT_GAME_GOAL_TILE_GOLD:
			return [UIImage imageNamed:@"Gold_Bars.png"];
		case TGT_GAME_BLOCKING_TILE_PIT_TRAP:
			return [UIImage imageNamed:@"Spikes_Empty.png"];
		case TGT_GAME_BLOCKING_TILE_HENCHMAN_OCCUPIED:
			return [UIImage imageNamed:@"Sprite_DR_1.png"];
		case TGT_GAME_WALKABLE_TILE_PIT_TRAP_OCCUPIED_EAST:
		case TGT_GAME_WALKABLE_TILE_PIT_TRAP_OCCUPIED_WEST:
		case TGT_GAME_WALKABLE_TILE_PIT_TRAP_OCCUPIED_SOUTH:
		case TGT_GAME_WALKABLE_TILE_PIT_TRAP_OCCUPIED_NORTH:
			return [UIImage imageNamed:@"Spikes_Occupied.png"];
			// default is show nothing...
		case TGT_GAME_WALKABLE_TILE_EMPTY:
		case TGT_GAME_WALKABLE_TILE_EMPTY_HIGHLIGHT:
		case TGT_GAME_BLOCKING_TILE_NONE:
		default:
			return nil;
	}
}


#pragma mark - init

- (void)setupInitialGridViewArray
{
	[super setupInitialGridViewArray];

	// uIState
	uIState_ = TGT_GAME_UI_STATE_IDLE;

	int gridTotal = self.game.gridHeight * self.game.gridWidth;

	// setup the dragCoordinateArray
	self.dragCoordinateDictArray = [[NSMutableArray alloc] initWithCapacity:gridTotal];

	// extraViews multi-array - stuffed with NSNull
	self.extraViews = [[NSMutableArray alloc] initWithCapacity:self.game.gridHeight];
	NSMutableArray *subarray;
	NSNull *null = [NSNull null];
	for (int y = 0; y < self.game.gridHeight; y++) {
		subarray = [NSMutableArray arrayWithCapacity:self.game.gridWidth];
		for (int x = 0; x < self.game.gridWidth; x++) {
			[subarray insertObject:null atIndex:x];
		}
		[self.extraViews insertObject:subarray atIndex:y];
	}

	// this will loop through and populate extraViews as necessary (in draw order)
	[self refreshViewPositionsAndStates];
}


@end
