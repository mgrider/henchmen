//
//  TGT_Game.h
//  Henchmen
//
//  Created by Martin Grider on 12/15/12.
//  Copyright (c) 2012 Abstract Puzzle. All rights reserved.
//

#import "GGM_BaseModel.h"


@interface TGT_Game : GGM_BaseModel


@property (assign) BOOL hasCutScene;
@property (strong) NSString *cutSceneText;

@property (assign) int allowedMinions;
@property (assign) int minionsUsed;

@property (assign) int entryX;
@property (assign) int entryY;


- (int)minionsRemaining;


@end
