//
//  TGT_GameConfig.m
//  Henchmen
//
//  Created by Martin Grider on 12/15/12.
//  Copyright (c) 2012 Abstract Puzzle. All rights reserved.
//

#import "TGT_GameConfig.h"
#import "TGT_GameConstants.h"
#import "TGT_Game.h"


@implementation TGT_GameConfig


+ (TGT_Game *)gameObjectForLevel:(int)level
{

	// level is not zero indexed

//	int maxWidthOrHeight = 10;
	int widthHeight = 8;

	// instantiate the game model
	TGT_Game *game = [TGT_Game instanceWithWidth:widthHeight andHeight:widthHeight];
	[game setMinionsUsed:0];

	// entryX and entryY
	game.entryX = 0;
	game.entryY = 4;

//	int tiles[maxWidthOrHeight][maxWidthOrHeight];
//	for (int x = 0; x < maxWidthOrHeight; x++) {
//		for (int y = 0; y < maxWidthOrHeight; y++) {
//			tiles[y][x] = 0;
//		}
//	}

	// set up special tiles
	// (and their associations?)
	switch (level) {
		case 1: {
			[game setAllowedMinions:2];
			[game setStateAtX:3 andY:3 toState:TGT_GAME_BLOCKING_TILE_PIT_TRAP];
			[game setStateAtX:3 andY:0 toState:TGT_GAME_BLOCKING_TILE_NONE];
			[game setStateAtX:3 andY:1 toState:TGT_GAME_BLOCKING_TILE_NONE];
			[game setStateAtX:3 andY:2 toState:TGT_GAME_BLOCKING_TILE_NONE];
			[game setStateAtX:3 andY:4 toState:TGT_GAME_BLOCKING_TILE_NONE];
			[game setStateAtX:3 andY:5 toState:TGT_GAME_BLOCKING_TILE_NONE];
			[game setStateAtX:3 andY:6 toState:TGT_GAME_BLOCKING_TILE_NONE];
			[game setStateAtX:3 andY:7 toState:TGT_GAME_BLOCKING_TILE_NONE];
			[game setStateAtX:5 andY:3 toState:TGT_GAME_GOAL_TILE_GOLD];
			break;
		}
			
		default:
			break;
	}

	// set up the cutscenes
	switch (level) {
		case 1: {
			game.hasCutScene = YES;
			game.cutSceneText = @"I've always wanted to change the world. I think I've finally found a place where I can do that.";
			break;
		}

		default:
			break;
	}

	return game;
}


@end
