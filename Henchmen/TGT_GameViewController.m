//
//  TGT_GameViewController.m
//  Henchmen
//
//  Created by Martin Grider on 12/15/12.
//  Copyright (c) 2012 Abstract Puzzle. All rights reserved.
//

#import "TGT_GameViewController.h"
#import "TGT_Game.h"
#import "TGT_GameView.h"
#import "TGT_GameConfig.h"
#import "TGT_Util.h"


@interface TGT_GameViewController ()

@property (strong, nonatomic) TGT_Game *game;

@end


@implementation TGT_GameViewController

@synthesize game = game_;
@synthesize gameView = gameView_;
@synthesize menuButton = menuButton_;
@synthesize minionCountLabel = minionCountLabel_;


#pragma mark - IBActions

- (IBAction)menuButtonPressed:(id)sender
{
	[self.navigationController popToRootViewControllerAnimated:YES];
}


#pragma mark - boilerplate

- (void)viewDidLoad
{
    [super viewDidLoad];

	// always start from level 1
	game_ = [TGT_GameConfig gameObjectForLevel:1];

//	[gameView_ setRecognizesDrags:YES];
	[gameView_ setGridType:GGM_GRIDTYPE_IMAGE];
//	[gameView_ setRecognizesTaps:YES];
	[gameView_ setRecognizesDrags:YES];

	[gameView_ setGame:game_];

	// fonts
	UIFont *font = [TGT_Util fontWithSize:50.0f];
	[[menuButton_ titleLabel] setFont:font];
	font = [TGT_Util fontWithSize:140.0f];
	[minionCountLabel_ setFont:font];

	// update minion count remaining
	[minionCountLabel_ setText:[NSString stringWithFormat:@"%i", game_.minionsRemaining]];
	[[NSNotificationCenter defaultCenter]
	 addObserverForName:kTGT_MinionCountChangedNotification
	 object:nil
	 queue:[NSOperationQueue mainQueue]
	 usingBlock:^(NSNotification *note) {
		 [minionCountLabel_ setText:[NSString stringWithFormat:@"%i", game_.minionsRemaining]];
	 }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
