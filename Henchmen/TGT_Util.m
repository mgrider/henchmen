//
//  TGT_Util.m
//  Henchmen
//
//  Created by Martin Grider on 12/16/12.
//  Copyright (c) 2012 Abstract Puzzle. All rights reserved.
//

#import "TGT_Util.h"


@implementation TGT_Util


+ (UIFont *)fontWithSize:(float)size
{
	UIFont *font = [UIFont fontWithName:@"VisitorTT1BRK" size:size];
	return font;
}


@end
