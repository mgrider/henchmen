//
//  TGT_GameViewController.h
//  Henchmen
//
//  Created by Martin Grider on 12/15/12.
//  Copyright (c) 2012 Abstract Puzzle. All rights reserved.
//

#import <UIKit/UIKit.h>


@class TGT_GameView;

@interface TGT_GameViewController : UIViewController


@property (weak, nonatomic) IBOutlet TGT_GameView *gameView;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UILabel *minionCountLabel;


- (IBAction)menuButtonPressed:(id)sender;


@end
