//
//  TGT_GameView.h
//  Henchmen
//
//  Created by Martin Grider on 12/15/12.
//  Copyright (c) 2012 Abstract Puzzle. All rights reserved.
//

#import "GGM_UIViewIsometric.h"
#import "TGT_GameConstants.h"


#define kTGT_MinionCountChangedNotification @"kTGT_MinionCountChangedNotification"


@class TGT_Game;

@interface TGT_GameView : GGM_UIViewIsometric


@property (strong) NSMutableArray *dragCoordinateDictArray;
@property (assign) TGT_Game_UI_StateType uIState;

@property (strong) NSMutableArray *extraViews;

@property (strong) TGT_Game *game;


@end
