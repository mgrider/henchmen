//
//  TGT_Util.h
//  Henchmen
//
//  Created by Martin Grider on 12/16/12.
//  Copyright (c) 2012 Abstract Puzzle. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TGT_Util : NSObject


+ (UIFont *)fontWithSize:(float)size;


@end
