//
//  TGT_GameConfig.h
//  Henchmen
//
//  Created by Martin Grider on 12/15/12.
//  Copyright (c) 2012 Abstract Puzzle. All rights reserved.
//

#import <Foundation/Foundation.h>


@class TGT_Game;

@interface TGT_GameConfig : NSObject


+ (TGT_Game *)gameObjectForLevel:(int)level;


@end
