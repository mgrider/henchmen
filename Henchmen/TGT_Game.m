//
//  TGT_Game.m
//  Henchmen
//
//  Created by Martin Grider on 12/15/12.
//  Copyright (c) 2012 Abstract Puzzle. All rights reserved.
//

#import "TGT_Game.h"


@implementation TGT_Game

@synthesize hasCutScene = hasCutScene_;
@synthesize cutSceneText = cutSceneText_;
@synthesize allowedMinions = allowedMinions_;
@synthesize minionsUsed = minionsUsed_;
@synthesize entryX = entryX_;
@synthesize entryY = entryY_;


- (int)minionsRemaining
{
	return allowedMinions_ - minionsUsed_;
}


@end
